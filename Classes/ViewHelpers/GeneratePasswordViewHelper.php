<?php
namespace DKM\SiteGeneratorExtended\ViewHelpers;

use DKM\SiteGeneratorExtended\Utility\MiscUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class GeneratePasswordViewHelper extends AbstractViewHelper
{

    /**
     * @param array $arguments
     * @param \Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return mixed|string
     */
    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        return MiscUtility::generatePassword();
    }
}
