<?php

namespace DKM\SiteGeneratorExtended\Dto;

class SiteGeneratorDto extends \Oktopuce\SiteGenerator\Dto\SiteGeneratorDto
{


    protected array $firstStep = [];

    protected array $secondStep = [];

    protected int $beUserId;

    /**
     * @return array
     */
    public function getFirstStep(): array
    {
        return $this->firstStep;
    }

    /**
     * @param array $firstStep
     */
    public function setFirstStep(array $firstStep): void
    {
        $this->firstStep = $firstStep;
    }

    /**
     * @return array
     */
    public function getSecondStep(): array
    {
        return $this->secondStep;
    }

    /**
     * @param array $secondStep
     */
    public function setSecondStep(array $secondStep): void
    {
        $this->secondStep = $secondStep;
    }

    public function __call( string $methodName, array $arguments) {
        if (strpos($methodName, 'get') === 0 && strlen($methodName) > 4) {
            $propertyName = lcfirst(substr($methodName, 3));
            $array = array_merge($this->firstStep, $this->secondStep);
            return $array[$propertyName] ?? null;
        }
    }

    /**
     * @return int
     */
    public function getBeUserId(): int
    {
        return $this->beUserId;
    }

    /**
     * @param int $beUserId
     */
    public function setBeUserId(int $beUserId): void
    {
        $this->beUserId = $beUserId;
    }


}