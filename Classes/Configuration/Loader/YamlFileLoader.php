<?php
namespace DKM\SiteGeneratorExtended\Configuration\Loader;

use DKM\SiteGeneratorExtended\Dto\SiteGeneratorDto;
use DKM\SiteGeneratorExtended\Utility\MiscUtility;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\Exception\MissingArrayPathException;

/**
 * Class YamlFileLoader
 * @package DKM\SiteGeneratorExtended
 */
class YamlFileLoader extends \TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader
{
    /**
     * @var array
     */
    protected array $siteGeneratorConfiguration = [];

    /**
     * @var SiteGeneratorDto
     */
    protected SiteGeneratorDto $siteGeneratorSiteData;

    /**
     * @var array
     */
    protected array $siteGeneratorCopyMap = [];

    /**
     * @var array
     */
    protected array $siteGeneratorSpecialData = [];

    /**
     * YamlFileLoader constructor.
     * @param $siteGeneratorConfiguration
     * @param $siteGeneratorSiteData
     * @param $siteGeneratorCopyMap
     */
    public function __construct($siteGeneratorConfiguration, $siteGeneratorSiteData , $siteGeneratorCopyMap, $siteGeneratorSpecialData)
    {
        $this->setSiteGeneratorConfiguration($siteGeneratorConfiguration);
        $this->setSiteGeneratorSiteData($siteGeneratorSiteData);
        $this->setSiteGeneratorCopyMap($siteGeneratorCopyMap);
        $this->setSiteGeneratorSpecialData($siteGeneratorSpecialData);
    }

    /**
     * @return array
     */
    public function getSiteGeneratorConfiguration(): array
    {
        return $this->siteGeneratorConfiguration;
    }

    /**
     * @param $key
     * @return string
     */
    public function getSiteGeneratorConfigurationValue($key): string
    {
        if(isset($this->getSiteGeneratorConfiguration()[$key])) {
            return $this->getSiteGeneratorConfiguration()[$key];
        } else {
            MiscUtility::addFlashMessage(
                'The element of $sysConfig[\'' . $key . '\'] does not exist',
                'Warning',
                AbstractMessage::WARNING
            );
        }
        return '';
    }


    /**
     * @param array $siteGeneratorConfiguration
     */
    public function setSiteGeneratorConfiguration(array $siteGeneratorConfiguration): void
    {
        $this->siteGeneratorConfiguration = $siteGeneratorConfiguration;
    }

    /**
     * @return SiteGeneratorDto
     */
    public function getSiteGeneratorSiteData(): SiteGeneratorDto
    {
        return $this->siteGeneratorSiteData;
    }

    /**
     * @param $key
     * @return string
     */
    public function getSiteGeneratorSiteDataValue($key): string
    {
        $method = 'get' . ucfirst($key);
        return $this->getSiteGeneratorSiteData()->{$method}();
//        if(isset($this->getSiteGeneratorSiteData())) {
//            return $this->getSiteGeneratorSiteData()[$key];
//        } else {
//            MiscUtility::addFlashMessage(
//                'Ext:site_generator_extended: the element of $data[\'' . $key . '\'] does not exist',
//                'Warning',
//                AbstractMessage::WARNING
//            );
//        }
//        return '';
    }

    /**
     * @param SiteGeneratorDto $siteGeneratorSiteData
     */
    private function setSiteGeneratorSiteData(SiteGeneratorDto $siteGeneratorSiteData): void
    {
        $this->siteGeneratorSiteData = $siteGeneratorSiteData;
    }

    /**
     * @return array
     */
    private function getSiteGeneratorCopyMap(): array
    {
        return $this->siteGeneratorCopyMap;
    }

    /**
     * @param $table
     * @param $uid
     * @return string
     */
    public function getSiteGeneratorCopyMapValue($table, $uid): string
    {
        if (!is_numeric($uid) && is_string($uid)) {
            try {
                $uid = (string)ArrayUtility::getValueByPath($GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS'], $uid,'.');
            } catch (MissingArrayPathException $exception) {
                return '';
            }
        }
        if (isset($this->getSiteGeneratorCopyMap()[$table][$uid])) {
            return $this->getSiteGeneratorCopyMap()[$table][$uid];
        } else {
            MiscUtility::addFlashMessage(
                'The value $copyMappingArray[' . $table . '][' . $uid . '] does not seem to exist in the copyMappingArray.',
                'Warning',
                AbstractMessage::WARNING
            );
        }
        return '';
    }


    /**
     * @param array $siteGeneratorCopyMap
     */
    private function setSiteGeneratorCopyMap(array $siteGeneratorCopyMap): void
    {
        $this->siteGeneratorCopyMap = $siteGeneratorCopyMap;
    }

    /**
     * @return array
     */
    public function getSiteGeneratorSpecialData(): array
    {
        return $this->siteGeneratorSpecialData;
    }

    /**
     * @param $key
     * @return string
     */
    public function getSiteGeneratorSpecialDataValue($key): string
    {
        if(isset($this->getSiteGeneratorSpecialData()[$key])) {
            return $this->getSiteGeneratorSpecialData()[$key];
        } else {
            MiscUtility::addFlashMessage(
                'Ext:site_generator_extended: the element of $specialData[\'' . $key . '\'] does not exist',
                'Warning',
                AbstractMessage::WARNING
            );
        }
        return '';
    }


    /**
     * @param array $siteGeneratorSpecialData
     */
    public function setSiteGeneratorSpecialData(array $siteGeneratorSpecialData): void
    {
        $this->siteGeneratorSpecialData = $siteGeneratorSpecialData;
    }

    /**
     * Main function that gets called recursively to check for %...% placeholders
     * inside the array
     *
     * @param array $content the current sub-level content array
     * @param array $referenceArray the global configuration array
     *
     * @return array the modified sub-level content array
     */
    protected function processPlaceholders(array $content, array $referenceArray): array
    {
        foreach ($content as $k => $v) {
            if (is_array($v)) {
                $content[$k] = $this->processPlaceholders($v, $referenceArray);
            } elseif ($this->isSiteGeneratorPlaceholder($v)) {
                $content[$k] = $this->getValueFromSiteGenerator($v);
            } elseif ($this->containsPlaceholder($v)) {
                $content[$k] = $this->processPlaceholderLine($v, $referenceArray);
            }
        }
        return $content;
    }


    /**
     * Return value from Site Generator
     *
     * Environment variables may only contain word characters and underscores (a-zA-Z0-9_)
     * to be compatible to shell environments.
     *
     * @param string $value
     * @return string
     */
    private function getValueFromSiteGenerator(string $value): string
    {
        $matches = [];
        preg_match_all('/%sitegenerator\([\'"]?([:,\.\w]+)[\'"]?\)%/', $value, $matches);
        $envVars = array_combine($matches[0], $matches[1]);
        foreach ($envVars as $substring => $placeholderValue) {
            list($key, $value) = explode(':', $placeholderValue, 2);
            switch ($key) {
                case 'copymap':
                    list($table, $id) = explode(':', $value);
                    return $this->getSiteGeneratorCopyMapValue($table, $id);
                case 'config':
                    return $this->getSiteGeneratorConfigurationValue($value) ?: '';
                case 'data':
                    return $this->getSiteGeneratorSiteDataValue($value) ?: '';
                default:
                    return $this->getSiteGeneratorSpecialDataValue($key) ?: '';
            }
        }
        return $value;
    }


    /**
     * Checks if a value is a string and contains an Site Generator placeholder
     *
     * @param mixed $value the probe to check for
     * @return bool
     */
    protected function isSiteGeneratorPlaceholder($value): bool
    {
        return is_string($value) && (strpos($value, '%sitegenerator(') !== false);
    }

    /**
     * By-pass parent processImport - which checks for the special "imports" key on the main level of a file,
     * which calls "load" recursively.
     * @param array $content
     * @param string|null $fileName
     *
     * @return array
     */
    protected function processImports(array $content, ?string $fileName): array
    {
        return $content;
    }
}