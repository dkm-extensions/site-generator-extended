<?php
/**
 * Created by PhpStorm.
 * User: stigfaerch
 * Date: 15/01/2019
 * Time: 11.48
 */

namespace DKM\SiteGeneratorExtended\Configuration;


use DKM\Freesite\Service\MiscService;
use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class SiteConfiguration extends \TYPO3\CMS\Core\Configuration\SiteConfiguration
{
    /**
     * @return array
     */
    public function getAllSiteConfiguration() {
        return $this->getAllSiteConfigurationFromFiles();
    }

    /**
     * @param $value
     * @return string
     */
    public function getIdentifier($value) {
        $value = self::createSlug($value);
        $fileName = $this->configPath . '/' . $value . '/' . $this->configFileName;
        $index = 0;
        while(file_exists($fileName)) {
            $fileName = $this->configPath . '/' . $value . $index++ . '/' . $this->configFileName;
            $fileName = "{$this->configPath}/{$value}-{$index}/$this->configFileName";
            $index++;
        }
        if($index) return $value . "-" . $index;
        return $value;
    }

    /**
     * @param $string
     * @return null|string|string[]
     */
    static public function createSlug($string) {
        $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
        return strtolower($slug);
    }

}