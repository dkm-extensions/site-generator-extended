<?php

namespace DKM\SiteGeneratorExtended\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MigrateWebsiteTitleCommand extends Command
{
    protected function configure()
    {
        $this->setHelp('Run this to migrate website title from sys_template to site configuration.');
    }


    /**
     * Executes the command for migrating website title of sys_template to site configuration
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \TYPO3\CMS\Core\Configuration\Exception\SiteConfigurationWriteException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title($this->getDescription());
        $table = 'sys_template';
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
        $siteConfiguration = GeneralUtility::makeInstance(SiteConfiguration::class);
        $sites = $siteConfiguration->getAllExistingSites();
        foreach ($sites as $site) {

            $resultRows = $connection->select(['sitetitle'], $table, ['pid' => $site->getRootPageId(), 'root' => 1]);
            if($resultRows) {
                $websiteTitle = $resultRows->fetchOne();
                if($websiteTitle) {
                    $arrayMerged = array_merge($site->getConfiguration(), ['websiteTitle' => $websiteTitle]);
                    $siteConfiguration->write($site->getIdentifier(), $arrayMerged);
                    $io->writeln("Migrated: $websiteTitle (pid:{$site->getRootPageId()})");
                }
            }
        }
        return Command::SUCCESS;
    }

}