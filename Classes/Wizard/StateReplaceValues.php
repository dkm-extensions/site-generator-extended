<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Dto\SiteGeneratorDto;
use Oktopuce\SiteGenerator\Utility\ExtendedTemplateService;
use Oktopuce\SiteGenerator\Utility\TemplateDirectivesService;
use Oktopuce\SiteGenerator\Wizard\Event\UpdateTemplateHPEvent;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use Oktopuce\SiteGenerator\Wizard\StateUpdateTemplateHP;
use Psr\EventDispatcher\EventDispatcherInterface;
use TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationPathDoesNotExistException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use Oktopuce\SiteGenerator\Dto\BaseDto;

/**
 * StateCreateBeGroup
 */
class StateReplaceValues extends StateUpdateTemplateHP implements SiteGeneratorStateInterface
{

    protected $mappingArray = [];

    /**
     * Create BE user group
     *
     * @param SiteGeneratorWizard $context
     * @throws \Doctrine\DBAL\Driver\Exception
     * @return void
     */
    public function process(SiteGeneratorWizard $context): void
    {
        /** @var SiteGeneratorDto $siteData */
        $siteData = $context->getSiteData();
        $settings = $context->getSettings();
        //TODO Implement table source - requires https://github.com/Oktopuce/site_generator/issues/12
        // Then we can just use $siteData->getMappingArrayMerge() for all tables
        $this->mappingArray = [
            'pages' => $siteData->getMappingArrayMerge()['pages'],
            'be_users' => ['_' => $siteData->getBeUserId()],
            'be_groups' => ['_' => $siteData->getBeGroupId()]
        ];
        $this->replaceFieldValues($siteData, $settings);
        $this->updateTemplates($siteData);
        if(GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator_extended', 'enableDefaultPermissions')) {
            $this->setDefaultPermissions($siteData);
        }
    }

    /**
     * @param SiteGeneratorDto $siteData
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function setDefaultPermissions(SiteGeneratorDto $siteData) {
        $table = 'pages';
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
        $tsconfig = "TCEMAIN.permissions.userid = {$siteData->getBeUserId()}\nTCEMAIN.permissions.groupid = {$siteData->getBeGroupId()}\n";
        $tsconfig .= $connection->select(['TSconfig'], $table, ['uid' => $siteData->getHpPid()])->fetchOne();
        $connection->update($table, ['TSconfig' => $tsconfig], ['uid' => $siteData->getHpPid()]);
    }

    /**
     * @param SiteGeneratorDto $siteData
     * @param array $settings
     * @throws \TYPO3\CMS\Core\Configuration\Exception\ExtensionConfigurationExtensionNotConfiguredException
     */
    private function replaceFieldValues(SiteGeneratorDto $siteData, array $settings) {
        try {
            $replaceFieldContent = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator_extended', 'replaceFieldContent');
            foreach ((array)$replaceFieldContent as $table => $target) {
                $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
                foreach ($target as $sourceUid => $typeAndData) {
                    $replaceArr = [];
                    if($sourceUid == 'all') {
                        $replaceArr = $this->mappingArray[$table];
                    } else {
                        if(isset($this->mappingArray[$table][$sourceUid])) {
                            $replaceArr = [$sourceUid => $this->mappingArray[$table][$sourceUid]];
                        }
                    }
                    foreach ($replaceArr as $srcUid => $newUid) {
                        [$type, $data] =  explode(':', current($typeAndData));
                        switch ($type) {
                            case 'siteData':
                                $method = 'get' . ucfirst($data);
                                $connection->update($table,[key($typeAndData) => $siteData->{$method}()], ['uid' => $newUid]);
                            // todo
//                            case 'translateUid':
                        }
                    }
                }
            }
        } catch (ExtensionConfigurationPathDoesNotExistException $exception) {

        }
    }

    /**
     * Update site template to set new uids
     *
     * @param BaseDto $siteData New site data
     * @throws \Doctrine\DBAL\Driver\Exception
     * @return void
     */
    protected function updateTemplates(BaseDto $siteData): void
    {
        // Get the row of the first VISIBLE template of the page. where clause like the frontend.
        $templateRow = $this->templateService->ext_getFirstTemplate($siteData->getHpPid());

        $updateConfigurations = [
            ['table' => 'sys_template', 'field' => 'constants', 'uids' => (!empty($templateRow) ? [$templateRow['uid']] : [])],
            ['table' => 'be_groups', 'field' => 'TSconfig', 'uids' => [$siteData->getBeGroupId()]],
            ['table' => 'pages', 'field' => 'TSconfig', 'uids' => $this->mappingArray['pages']]
        ];

        foreach ($updateConfigurations as $config) {
            $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($config['table']);
            if ($config['uids']) {
                foreach ($config['uids'] as $uid) {
                    $content = $connection->select([$config['field']], $config['table'], ['uid' => $uid])->fetchOne();

                    if($content) {
                        // The value modification operator will not be found as a typoscript object, and will not be available in the objReg.
                        // Therfore we convert := to = and back before saving is done
                        $lines = preg_split("%\R%", $content);
                        // Find lines with value modification operator
                        $valueModificationLines = array_filter($lines, function($v) { return strpos($v, ':=');});
                        $content = str_replace(':=', '=', $content);

                        $this->templateService->ext_regObjectPositions($content);
                        $objReg = $this->templateService->getObjReg();

                        foreach ($objReg as $key => $rawP) {

                            $this->templateDirectivesService->lookForDirectives(($rawP > 0 ? $this->templateService->raw[$rawP - 1] : ''));
                            $table = $this->templateDirectivesService->getTable('pages');

                            $value = GeneralUtility::trimExplode('=', $this->templateService->raw[$rawP]);
                            $uidsToExclude = GeneralUtility::trimExplode(',',
                            $this->templateDirectivesService->getIgnoreUids(), true);
                            $filteredMapping = $mapping = $siteData->getMappingArrayMerge($table);

                            // Manage uids to exclude
                            if (!empty($uidsToExclude)) {
                                $filteredMapping = array_filter($mapping, static function ($key) use ($uidsToExclude) {
                                    return !in_array((string)$key, $uidsToExclude, true);
                                }, ARRAY_FILTER_USE_KEY);
                            }

                            $action = $this->templateDirectivesService->getAction('mapInList');
                            $updatedValue = '';

                            switch ($action) {
                                case 'mapInList' :
                                    if(preg_match('/^(\D*\()([\d,]+)(?:,\d+)*(\))*/', $value[1], $matches)) {
                                        $value[1] = $matches[2];
                                    };
                                    $updatedValue = ($matches[1] ?? '') . $this->mapInList($value[1], $filteredMapping) . ($matches[3] ?? '');
                                    break;
                                case 'mapInString' :
                                    $updatedValue = $this->mapInString($value[1], $filteredMapping);
                                    break;
                                case 'exclude' :
                                    // Exclude all line
                                    break;
                                default :
                                    // Call custom action if there is one
                                    $parameters = $this->templateDirectivesService->getParameters();
                                    $event = $this->eventDispatcher->dispatch(new UpdateTemplateHPEvent($action, $parameters, $value[1], $filteredMapping, $this->templateDirectivesService));
                                    $updatedValue = $event->getUpdatedValue();
                                    break;
                            }

                            if (!empty($updatedValue)) {
                                $this->templateService->ext_putValueInConf($key, $updatedValue);
                            }
                        }

                        if ($this->templateService->changed) {
                            // Set the data to be saved
                            $recData = [];
                            $saveId = $uid;
                            // Restore the value modification operators (:=)
                            foreach ($valueModificationLines as $index => $line) {
                                $this->templateService->raw[$index] = str_replace('=', ':=', $this->templateService->raw[$index]);
                            }
                            $this->templateService->raw = array_filter($this->templateService->raw, function ($v) {
                                return strpos($v, '# ext=SiteGenerator;') === false;
                            });
                            $recData[$config['table']][$saveId][$config['field']] = implode(LF, $this->templateService->raw);
                            // Create new  tce-object
                            $tce = GeneralUtility::makeInstance(DataHandler::class);
                            $tce->start($recData, []);
                            $tce->process_datamap();

                            $this->log(LogLevel::NOTICE, 'Update home page template with new pid done');
                        }
                    }
                }
            }
        }
    }
}