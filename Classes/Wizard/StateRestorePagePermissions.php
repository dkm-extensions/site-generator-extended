<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Dto\SiteGeneratorDto;
use Doctrine\DBAL\Connection as ConnectionAlias;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * StateRestorePermissions - restore page permissions from template site
 */
class StateRestorePagePermissions extends StateBase implements SiteGeneratorStateInterface
{

    /**
     * Restore all page permissions as defined in site template
     *
     * @param SiteGeneratorWizard $context
     * @return void
     */
    public function process(SiteGeneratorWizard $context): void
    {
        /** @var SiteGeneratorDto $siteData */
        $siteData = $context->getSiteData();
        $this->restorePagePermissions($siteData);
    }

    /**
     * @param SiteGeneratorDto $siteData
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    protected function restorePagePermissions(SiteGeneratorDto $siteData) {
        $copyMappingPagesArray = $siteData->getMappingArrayMerge()['pages'];
        $table = 'pages';
        $select_fields = ['uid','slug','perms_userid','perms_groupid','perms_user','perms_group','perms_everybody'];
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $queryBuilder = $connectionPool->getQueryBuilderForTable($table)->from($table);
        $queryBuilder->select(...$select_fields)
            ->where($queryBuilder->expr()->in('uid', $queryBuilder->createNamedParameter(array_keys($copyMappingPagesArray), ConnectionAlias::PARAM_INT_ARRAY)));
        $rows = $queryBuilder->execute()->fetchAllAssociativeIndexed();
        foreach ($rows as $uid => $row) {
            $updateArray = array('perms_user' => $row['perms_user'],
                'perms_group' => $row['perms_group'],
                'perms_everybody' => $row['perms_everybody']
            );
            if ($row['perms_userid'] == 0) {
                $updateArray['perms_userid'] = 0;
            }

//            if($this->data['SLUG']) {
//                $updateArray['slug'] = Utility::sanitizePath($this->data['SLUG'] . $row['slug']);
//            }
//            //ensure correct group permission on pages.
//            if (!$this->sysConfig['uid_pagePermsGroup']) {
//                $updateArray['perms_groupid'] = $this->userGroup_uid;
//            } elseif ($row['perms_groupid'] == 0) {
//                $updateArray['perms_groupid'] = 0;
//            }
            // Done on earlier step
            // if ($copyMappingPagesArray[$uid] != $siteData->getHpPid()) $updateArray['perms_userid'] = $this->user_uid;
            // Set no owner if original page had no owner

            $connection = $connectionPool->getConnectionForTable($table);
            $connection->update('pages', $updateArray, ['uid' => $copyMappingPagesArray[$uid]]);
        }
        // @extensionScannerIgnoreLine
        $siteData->addMessage($this->translate('generate.success.restorePagePermissions', [], 'site_generator_extended'));
    }
}
