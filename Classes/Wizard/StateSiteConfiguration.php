<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Configuration\Loader\YamlFileLoader;
use DKM\SiteGeneratorExtended\Dto\SiteGeneratorDto;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use TYPO3\CMS\Backend\Exception\SiteValidationErrorException;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Configuration\SiteConfiguration;
use TYPO3\CMS\Core\Log\LogLevel;
use Oktopuce\SiteGenerator\Domain\Repository\PagesRepository;

/**
 * StateSiteConfiguration
 */
class StateSiteConfiguration extends StateBase implements SiteGeneratorStateInterface
{
    protected array $extensionConfiguration;

    protected array $copyMappingArray = [];

    /**
     */
    public function __construct()
    {
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator');
        $extensionConfigurationExteneded = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator_extended');
        $this->extensionConfiguration = array_merge_recursive($extensionConfiguration, $extensionConfigurationExteneded);
        parent::__construct();
    }


    /**
     * Create site management
     *
     * @param SiteGeneratorWizard $context
     * @return void
     */
    public function process(SiteGeneratorWizard $context): void
    {
        /** @var SiteGeneratorDto $siteData */
        $siteData = $context->getSiteData();
        $this->copyMappingArray = $siteData->getMappingArrayMerge();
        // Create the domain name on first page
        $this->createSiteConfiguration($siteData, $context->getSettings());
    }

    /**
     * Create a site configuration
     *
     * @param SiteGeneratorDto $siteData New site data
     * @param array $settings
     * @throws \TYPO3\CMS\Core\Configuration\Exception\SiteConfigurationWriteException
     */
    protected function createSiteConfiguration(SiteGeneratorDto $siteData, array $settings): void
    {
        if (!empty($siteData->getDomain())) {

            /** @var PagesRepository $pagesRepository */
            $pagesRepository = GeneralUtility::makeInstance(PagesRepository::class);
            $uids = $siteData->getMappingArrayMerge()['pages'];
            $rootSiteId = $pagesRepository->getRootSiteId($uids);

            if ($rootSiteId) {
                try {
                    // Get extension configuration
                    $extensionConfiguration = $this->getExtensionConfiguration();

                    $newSiteConfiguration = [];
                    $newSiteConfiguration['rootPageId'] = $rootSiteId;
                    $newSiteConfiguration['base'] = $siteData->getDomain();
                    $newSiteConfiguration['websiteTitle'] = $siteData->getTitle();
                    $newSiteConfiguration['baseVariants'] = [];
                    $this->addDefaultSettings($newSiteConfiguration);
                    $this->addBaseVariants($newSiteConfiguration, $siteData);
                    $this->addLanguageSettings($newSiteConfiguration);
                    $this->add404Configuration($newSiteConfiguration, $siteData);
                    $this->addRouteCookieInformation($newSiteConfiguration);
                    $this->addFromTemplate($newSiteConfiguration, $siteData, $settings);
                    $siteIdentifier = $extensionConfiguration['siteIdentifierPrefix'] . $rootSiteId;

                    // Persist the configuration
                    /** @var SiteConfiguration $siteConfigurationManager */
                    $siteConfigurationManager = GeneralUtility::makeInstance(SiteConfiguration::class, Environment::getConfigPath() . '/sites');
                    $siteConfigurationManager->write($siteIdentifier, $newSiteConfiguration);

                    $this->log(LogLevel::NOTICE, 'Site configuration created');
                    // @extensionScannerIgnoreLine
                    $siteData->addMessage($this->translate('generate.success.createSiteConfiguration', [
                        $siteData->getDomain()
                    ]));
                } catch (SiteValidationErrorException $e) {
                    $this->log(LogLevel::ERROR, 'Cannont create site configuration for domain : ' . $siteData->getDomain());
                    throw new \Exception($this->translate('wizard.createSiteConfiguration.error'));
                }
            } else {
                $this->log(LogLevel::WARNING, 'The selected model does not contains root pages, no site configuration created');
                // @extensionScannerIgnoreLine
                $siteData->addMessage($this->translate('wizard.createSiteConfiguration.error.noRooTPage'));
            }
        }
    }

    /**
     * @param $newSiteConfiguration
     * @param SiteGeneratorDto $siteData
     */
    private function addBaseVariants(&$newSiteConfiguration, SiteGeneratorDto $siteData) {
        $siteDomain = $siteData->getDomain();
        $siteDomain = strpos($siteDomain, 'http') === 0 ? $siteDomain : 'https://' . $siteDomain;
        $siteDomain = str_replace('www.', '', $siteDomain);
        $siteDomain = parse_url($siteDomain, PHP_URL_HOST);
        $subdomainName = strtok($siteDomain, '.');
        if($this->extensionConfiguration['siteConfiguration']['baseVariants'] ?? false) {
            foreach ($this->extensionConfiguration['siteConfiguration']['baseVariants'] as $contextKey => $variantCfg) {
                if(empty($variantCfg['domain'])) continue;
                $contextDomain = strpos($variantCfg['domain'], 'http') === 0 ? $variantCfg['domain'] : 'https://' . $variantCfg['domain'];
                $domainParsed = parse_url($contextDomain);
                $newSiteConfiguration['baseVariants'][] = [
                    'base' => "{$domainParsed['scheme']}://$subdomainName.{$domainParsed['host']}",
                    'condition' => "applicationContext == \"{$variantCfg['context']}\""
                ];
            }
        }
    }

    /**
     * @param $newSiteConfiguration
     */
    private function addLanguageSettings(&$newSiteConfiguration)
    {
        if($this->extensionConfiguration['siteConfiguration']['includeLanguageSettings'] ?? false) {
            $newSiteConfiguration['languages']['0'] = [
                'title' => $this->extensionConfiguration['langTitle'],
                'enabled' => true,
                'base' => '/',
                'typo3Language' => 'default',
                'locale' => $this->extensionConfiguration['locale'],
                'iso-639-1' => $this->extensionConfiguration['iso-639-1'],
                'websiteTitle' => '',
                'navigationTitle' => $this->extensionConfiguration['navigationTitle'],
                'hreflang' => $this->extensionConfiguration['hreflang'],
                'direction' => $this->extensionConfiguration['direction'],
                'flag' => $this->extensionConfiguration['flag'],
                'languageId' => '0'
            ];
        }
    }

    /**
     * @param $newSiteConfiguration
     * @param SiteGeneratorDto $siteData
     */
    private function add404Configuration(&$newSiteConfiguration, SiteGeneratorDto $siteData) {
        if($this->extensionConfiguration['siteConfiguration']['404Pid'] ?? false) {
            $newSiteConfiguration['errorHandling'] = [];
            $newSiteConfiguration['errorHandling'][] = [
                'errorCode' => "404",
                'errorHandler' => 'Page',
                'errorContentSource' => 't3://page?uid=' . ($this->copyMappingArray['pages'][$this->extensionConfiguration['siteConfiguration']['404Pid']] ?? $this->extensionConfiguration['siteConfiguration']['404Pid'])
            ];
        }
    }

    /**
     * @param $newSiteConfiguration
     */
    private function addRouteCookieInformation(&$newSiteConfiguration) {
        if($cookiesInformationPid = $this->copyMappingArray['pages'][$this->extensionConfiguration['siteConfiguration']['cookiesInformationPid'] ?? false] ?? false) {
            $newSiteConfiguration['routes'][] = [
                'route' => $this->extensionConfiguration['siteConfiguration']['cookiesInformationRoute'] ?? '/cookieInfo/',
                'type' => 'uri',
                'source' => 't3://page?uid=' . $cookiesInformationPid
            ];
        }
    }

    /**
     * @param $newSiteConfiguration
     * @param SiteGeneratorDto $siteData
     * @param array $settings
     */
    private function addFromTemplate(&$newSiteConfiguration, SiteGeneratorDto $siteData, array $settings)
    {
        if ($this->extensionConfiguration['siteConfiguration']['template'] ?? false) {
            // TODO - Old Freesite code - All these should be available from siteData - REMOVE?
            $specialData = [
                //            'rootpageid' => $this->data['ROOTPAGEID'],
                //            'basedomain' => $this->getDomainFromData('DOMAIN'),
                //            'tempdomain' => $this->getDomainFromData(),
                //            'backenduser' => $this->userGroup_uid,
                //            'backendusergroup' => $this->user_uid,
            ];

            try {
                /** @var YamlFileLoader $yamlFileLoader */
                $yamlFileLoader = GeneralUtility::makeInstance(YamlFileLoader::class,
                    $settings,
                    $siteData,
                    $this->copyMappingArray,
                    $specialData
                );

                $siteConfigurationFromTemplate = $yamlFileLoader->load(GeneralUtility::getFileAbsFileName($this->extensionConfiguration['siteConfiguration']['template']));
                $newSiteConfiguration = array_merge($newSiteConfiguration, $siteConfigurationFromTemplate);
            } catch (\RuntimeException $exception) {
                //todo add error message
//                MiscService::errorMsg('msgErrYamlTemplateMissing');
            }
        }
    }

    /**
     * @param $newSiteConfiguration
     */
    private function addDefaultSettings(&$newSiteConfiguration)
    {
        if($this->extensionConfiguration['siteConfiguration']['defaults'] ?? false) {
            $newSiteConfiguration['imports'][] =  ['resource' => $this->extensionConfiguration['siteConfiguration']['defaults']];
        }
    }
}
