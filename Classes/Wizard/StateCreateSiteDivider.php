<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Dto\SiteGeneratorDto;
use Oktopuce\SiteGenerator\Utility\ExtendedTemplateService;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use Oktopuce\SiteGenerator\Dto\BaseDto;

/**
 * StateCreateBeGroup
 */
class StateCreateSiteDivider extends StateBase implements SiteGeneratorStateInterface
{

    protected $mappingArray = [];

    /**
     * Create BE user group
     *
     * @param SiteGeneratorWizard $context
     * @throws \Doctrine\DBAL\Driver\Exception
     * @return void
     */
    public function process(SiteGeneratorWizard $context): void
    {
        if(GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator_extended', 'createSiteDivider')) {
            /** @var SiteGeneratorDto $siteData */
            $siteData = $context->getSiteData();
            $this->createSiteDivider($siteData);
        }
    }

    /**
     * @param SiteGeneratorDto $siteData
     */
    private function createSiteDivider(SiteGeneratorDto $siteData) {
        // Create new  tce-object
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tce->start(['pages' => ['NEW_1' => [
                    'pid' => $siteData->getPid(),
                    'title' => $siteData->getTitle(),
                    'doktype' => 199,
                    'hidden' => 0]]],
            []);
        $tce->process_datamap();
        $siteDividerUid = $tce->substNEWwithIDs['NEW_1'];

        // Create new  tce-object
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tce->start([], ['pages' => [$siteData->getHpPid() => ['move' => $siteDividerUid]]]);
        $tce->process_cmdmap();
    }
}