<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Dto\SiteGeneratorDto;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Log\LogLevel;
use Oktopuce\SiteGenerator\Dto\BaseDto;
use Oktopuce\SiteGenerator\Domain\Repository\PagesRepository;

/**
 * StateSetPageBeGroup : set page access
 */
class StateSetPageBeUser extends StateBase implements SiteGeneratorStateInterface
{

    /**
     * Set group BE to all pages
     *
     * @param SiteGeneratorWizard $context
     * @return void
     */
    public function process(SiteGeneratorWizard $context): void
    {
        /** @var SiteGeneratorDto $siteData */
        $siteData = $context->getSiteData();
        // Affect BE group to pages created
        $this->setBeUser($siteData);
    }

    /**
     * Affect BE group to pages created
     *
     * @param SiteGeneratorDto $siteData New site data
     * @throws \Exception
     *
     * @return void
     */
    protected function setBeUser(SiteGeneratorDto $siteData): void
    {
        if ($siteData->getBeUserId()) {
            $pages = [];
            foreach ($siteData->getMappingArrayMerge()['pages'] as $sitePid) {
                $value = $sitePid == $siteData->getHpPid() ? 0 : $siteData->getBeUserId();
                /* @var $pagesRepository PagesRepository */
                $pagesRepository = GeneralUtility::makeInstance(PagesRepository::class);

                $updateValues = [
                    'perms_userid' => $value
                ];
                $pagesRepository->updatePage($sitePid, $updateValues);
                if($value) $pages[] = $sitePid;
            }
            $this->log(LogLevel::INFO, 'BE User #' . $siteData->getBeGroupId() . ' sets to pages');
            // @extensionScannerIgnoreLine
            $siteData->addMessage($this->translate('generate.success.beUserSetToPages', [$siteData->getBeUserId(), implode(',', $pages)], 'site_generator_extended'));
            $siteData->addMessage($this->translate('generate.success.beUserSetToPages.rootpage', [], 'site_generator_extended'));
        }
    }
}
