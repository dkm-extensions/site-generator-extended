<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Utility\MiscUtility;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderAccessPermissionsException;
use TYPO3\CMS\Core\Resource\Exception\InsufficientFolderWritePermissionsException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Log\LogLevel;
use Oktopuce\SiteGenerator\Dto\BaseDto;

/**
 * StateCreateBeUser
 */
class StateCreateBeUser extends StateBase implements SiteGeneratorStateInterface
{
    /**
     * @var ResourceFactory
     */
    private $resourceFactory;

    public function __construct(ResourceFactory $resourceFactory)
    {
        parent::__construct();
        $this->resourceFactory = $resourceFactory;
    }

    /**
     * Create BE user group
     *
     * @param SiteGeneratorWizard $context
     * @return void
     */
    public function process(SiteGeneratorWizard $context): void
    {
        $userUid = $this->createBeUser($context->getSiteData());
        $context->getSiteData()->setBeUserId($userUid);
    }

    /**
     * Create BE group with file mount, DB mount, access lists
     *
     * @param BaseDto $siteData New site data
     * @throws \Exception
     * @return int The uid of the user created
     */
    protected function createBeUser(BaseDto $siteData): int
    {
        // Get extension configuration
        $extensionConfiguration = $this->getExtensionConfiguration();

        // Create a new group with filemount at root page
        $data = [];
        $newUniqueId = 'NEW' . uniqid();
        $groupName = ($siteData->getGroupPrefix() ? $siteData->getGroupPrefix() . ' - ' : '') . $siteData->getTitle();
        $data['be_users'][$newUniqueId] = [
            'pid' => 0,
            'username' => strtolower($siteData->getUsername()),
            'email' => $siteData->getEmail(),
            'realName' => $siteData->getName(),
            'usergroup' => $siteData->getBeGroupId(),
            'password' => $siteData->getPassword(),
            'disable' => 0,
            'lang' => $extensionConfiguration['iso-639-1'] ?? 'default'
        ];
        $data['be_groups'][$siteData->getBeGroupId()] = [
            'title' => strtolower($siteData->getUsername())
        ];

        /* @var $tce DataHandler */
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tce->stripslashes_values = 0;
        $tce->start($data, []);
        $tce->process_datamap();

        // Retrieve uid of user group created
        $userId = $tce->substNEWwithIDs[$newUniqueId];

        if ($userId > 0) {
            $this->log(LogLevel::NOTICE, "Create BE user {$siteData->getUsername()} successful (uid = {$userId})");
            // @extensionScannerIgnoreLine
            $siteData->addMessage("- Create BE user {$siteData->getUsername()} with the password {$siteData->getPassword()} successful (uid = {$userId})");
        } else {
            $this->log(LogLevel::ERROR, 'Create BE user error');
            throw new \Exception('Create BE user error');
        }

        return ($userId);
    }
}
