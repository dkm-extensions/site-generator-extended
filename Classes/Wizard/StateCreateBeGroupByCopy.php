<?php

declare(strict_types=1);

/*
 *
 * This file is part of the "Site Generator" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 */

namespace DKM\SiteGeneratorExtended\Wizard;

use DKM\SiteGeneratorExtended\Utility\MiscUtility;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorStateInterface;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use Oktopuce\SiteGenerator\Wizard\StateBase;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use Oktopuce\SiteGenerator\Dto\BaseDto;

/**
 * StateCreateBeGroup
 */
class StateCreateBeGroupByCopy extends StateBase implements SiteGeneratorStateInterface
{
    /**
     * Create BE user group
     *
     * @param SiteGeneratorWizard $context
     * @return void
    */
    public function process(SiteGeneratorWizard $context): void
    {
        // Create BE group
        $groupId = $this->createBeGroup($context);

        $context->getSiteData()->setBeGroupId($groupId);
    }

    /**
     * Create BE group with file mount, DB mount, access lists
     *
     * @param SiteGeneratorWizard $context New site data
     * @throws \Exception
     * @return int The uid of the group created
     */
    protected function createBeGroup(SiteGeneratorWizard $context): int
    {
        $siteData = $context->getSiteData();
        // Get extension configuration
        $beGroupTemplateUid = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator_extended', 'beGroupTemplateUid');

        $table = 'be_groups';
        $connection = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable($table);
        if(!$groupTemplate = $connection->select(['pid', 'description','non_exclude_fields','allowed_languages','custom_options','pagetypes_select','tables_select', 'tables_modify', 'groupMods', 'file_mountpoints', 'file_permissions', 'TSconfig', 'subgroup', 'workspace_perms', 'category_perms'], $table, ['uid' => $beGroupTemplateUid])->fetchAssociative()) {
            throw new \Exception("BE group record template was not found from uid: '{$beGroupTemplateUid}'.");
        }

        // Create a new group with filemount at root page
        $data = [];
        $newUniqueId = 'NEW' . uniqid();
        $groupName = ($siteData->getGroupPrefix() ? $siteData->getGroupPrefix() . ' - ' : '') . $siteData->getTitle();

        $groupTemplate['db_mountpoints'] = $siteData->getHpPid();
        $groupTemplate['title'] = $groupName;

        $data['be_groups'][$newUniqueId] = $groupTemplate;

        // Set common mountpoint
        if ($siteData->getCommonMountPointUid()) {
            $data['be_groups'][$newUniqueId]['file_mountpoints'] = $siteData->getCommonMountPointUid();
        }

        // Set created mountpoint
        if ($siteData->getMountId()) {
            $data['be_groups'][$newUniqueId]['file_mountpoints'] .= ($siteData->getCommonMountPointUid() ? ',' : '') . $siteData->getMountId();
        }

        /* @var $tce DataHandler */
        $tce = GeneralUtility::makeInstance(DataHandler::class);
        $tce->stripslashes_values = 0;
        $tce->start($data, []);
        $tce->process_datamap();

        // Retrieve uid of user group created
        $groupId = $tce->substNEWwithIDs[$newUniqueId];


        // Update the TSconfig field
        $siteData->setBeGroupId($groupId);
        unset($data);
        $data['be_groups'][$groupId] = [
            'TSconfig' => $groupTemplate['TSconfig'] . "\noptions.defaultUploadFolder = " . MiscUtility::getSiteFolderCombinedIdentifier($context)
        ];
        $tce->start($data, []);
        $tce->process_datamap();

        if ($groupId > 0) {
            $this->log(LogLevel::NOTICE, 'Create BE group successful (uid = ' . $groupId);
            // @extensionScannerIgnoreLine
            $siteData->addMessage($this->translate('generate.success.beGroupCreated', [$groupName, $groupId]));
        }
        else {
            $this->log(LogLevel::ERROR, 'Create BE group error');
            throw new \Exception($this->translate('wizard.beGroup.error'));
        }

        return ($groupId);
    }

    /**
     * Get data from extension configuration
     *
     * @return array
     */
    public function getExtensionConfiguration(): array
    {
        return ($this->extensionConfiguration == null ? $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['site_generator_extended'] : []);
    }

}
