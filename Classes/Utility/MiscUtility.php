<?php
namespace DKM\SiteGeneratorExtended\Utility;

use Oktopuce\SiteGenerator\Dto\BaseDto;
use Oktopuce\SiteGenerator\Wizard\SiteGeneratorWizard;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class MiscUtility {
    /**
     * Method to generate random password
     * @param $settings
     * @return string
     */
    static function generatePassword(): string
    {
        $passwordConfig = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('site_generator_extended', 'password');
        $replace = function (&$password, $type) use ($passwordConfig) {
            static $passwordRange;
            if(!$passwordRange) {
                $passwordRange = range(0, $passwordConfig['length']-1);
                shuffle($passwordRange);
            }
            $password = substr_replace($password, $passwordConfig[$type][(rand() % strlen($passwordConfig[$type]))], next($passwordRange), 1);
        };
        $strength = $passwordConfig['strength'] ?? 1;
        //@todo stronger and up-to-date password
        $vowels = $passwordConfig['vowels'] ?? 'aeuy';
        $consonants = $passwordConfig['consonants'] ?? 'bdghjmnpqrstvz';
        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < ($passwordConfig['length'] ?? 8); $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        if ($strength & 1) {
            $replace($password, 'consonants_uppercase');
        }
        if ($strength & 2) {
            $replace($password, 'vowels_uppercase');
        }
        if ($strength & 4) {
            $replace($password, 'numbers');
        }
        if ($strength & 8) {
            $replace($password, 'otherchars');
        }
        return $password;
    }


    /**
     * @param $text
     * @param $header
     * @param $severity
     */
    public static function addFlashMessage($text, $header, $severity) {
        $message = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(FlashMessage::class,
            $text,
            $header, // [optional] the header
            AbstractMessage::WARNING, // [optional] the severity defaults to \TYPO3\CMS\Core\Messaging\FlashMessage::OK
            true // [optional] whether the message should be stored in the session or only in the \TYPO3\CMS\Core\Messaging\FlashMessageQueue object (default is false)
        );
        $flashMessageService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(FlashMessageService::class);
        $messageQueue = $flashMessageService->getMessageQueueByIdentifier();
        $messageQueue->addMessage($message);
    }


    /*
    * @return mixed|string
    * @throws \Exception
    */
    private static function getStorageUidFromGroupHomePath() {
        return self::getGroupHomePathArray()[0];
    }

    /**
     * @return mixed|string
     * @throws \Exception
     */
    private static function getFolderFromGroupHomePath() {
        return trim(self::getGroupHomePathArray()[1], '/');
    }

    /**
     * @return false|string[]
     * @throws \Exception
     */
    private static function getGroupHomePathArray() {
        $groupHomePathArray = explode(':', $GLOBALS['TYPO3_CONF_VARS']['BE']['groupHomePath']);
        if (count($groupHomePathArray) === 2 && is_numeric($groupHomePathArray[0])) {
            return $groupHomePathArray;
        } else {
            throw new \Exception('The Installation-Wide Option [BE][groupHomePath] was not configured correctly. Should be a combined folder identifier. Eg. 2:groups/');
        }
    }

    /**
     * @param $siteData
     * @return string
     * @throws \Exception
     */
    public static function getSiteFolder($siteData): string
    {
        if($siteData->getBeGroupId()) {
            return (string) $siteData->getBeGroupId();
        } else {
            throw new \Exception('The extension configuration siteFolderName was set to userGroupUid, but the usergroup uid was not found. Please check order of the states. StateCreateBeGroup should come before StateCreateGroupHomeFolder.');
        }
    }

    public static function getBaseFolderName(BaseDto $siteData) {
        return self::getFolderFromGroupHomePath();
    }

    /**
     * @param SiteGeneratorWizard $context
     * @return int|void
     * @throws \Exception
     */
    public static function getStorageUid(SiteGeneratorWizard $context) {
        return self::getStorageUidFromGroupHomePath();
}

    /**
     * @param SiteGeneratorWizard $context
     * @return string
     * @throws \Exception
     */
    public static function getSiteFolderCombinedIdentifier(SiteGeneratorWizard $context) {
        $siteData = $context->getSiteData();
        return self::getStorageUid($context) . ':' . self::getBaseFolderName($siteData) . '/' . self::getSiteFolder($siteData) . '/';
    }

}