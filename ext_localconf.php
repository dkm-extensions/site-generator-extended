<?php
//$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['site_generator_extended']['replaceFieldContent'] = [
//    Example
//    '{table}' => ['{origUid/all}' => ['{field}' => 'siteData:{someProperty}']]
//    'pages' => [31 => ['tx_myext_googleAnalyticsId' => 'siteData:googleAnalyticsId']]
//];

$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['site_generator_extended']['siteConfiguration']['baseVariants']['development']['context'] = 'Development';
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['site_generator_extended']['siteConfiguration']['baseVariants']['staging']['context'] = 'Production/Staging';
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['site_generator_extended']['siteConfiguration']['baseVariants']['testing']['context'] = 'Production/Testing';
$GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['site_generator_extended']['siteConfiguration']['baseVariants']['upgrade']['context'] = 'Production/Upgrade';