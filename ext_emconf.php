<?php
$EM_CONF[$_EXTKEY] = array(
	'title' => 'Site Generator Extended',
	'description' => 'Extended functionality for Site Generator',
	'category' => 'misc',
	'shy' => 0,
	'version' => '0.0.3',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'author' => 'Stig Nørgaard Færch',
	'author_email' => 'snf@dkm.dk',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);
