############################
### CUSTOM SUBCATEGORIES ###
###########################
# customsubcategory=100=Pid
# customsubcategory=110=Uid
# customsubcategory=120=Form
# customsubcategory=130=Label
# customsubcategory=140=Folder
# customsubcategory=150=BE Group access lists
# customsubcategory=160=Site configuration
# customsubcategory=170=Password configuration

# cat=Basic/110/10_BeGroupTemplateUid; type=int; label=BE user group template UID: This is the uid of the user group used as template for the new user group
beGroupTemplateUid = 0

# cat=Basic/110/20_EnableDefaultPermissions; type=boolean; label=Enable default permissions: This inserts page TypoScript on the root page to hardcode permissions for new pages
enableDefaultPermissions = 0

# cat=Basic/110/20_CreateSiteDivider; type=boolean; label=Create site divider: If enabled a page of the divider type is created, which the site will be moved to
createSiteDivider = 0

# cat=Folder/140/10_SiteFolder; type=options[Website title=siteTitle,Backend usergroup uid=userGroupUid]; label=Site folder: The name for site folder. Group uid or site title.
siteFolderName = siteTitle

# cat=Password/170/10_length; type=int; label=Password length: Length of generated password.
password.length = 8
# cat=Password/170/20_strength; type=int; label=Password strength: Strength values from 0 to 15. A combination of four different methods. 1:uppercase consonants, 2:uppercase vowels, 4:numbers, 8:other chars. If you want a combination of uppercase consonants and numbers, then you choose 5 (1+4).
password.strength = 15
# cat=Password/170/30_consonants; type=string; label=Consonants
password.consonants = bdghjmnpqrstvz
# cat=Password/170/40_consonants_uppercase; type=string; label=Consonants
password.consonants_uppercase = BDGHJLMNPQRSTVWXZ
# cat=Password/170/50_vowels; type=string; label=Vowels
password.vowels = aeuy
# cat=Password/170/60_vowels_uppercase; type=string; label=Vowels uppercase
password.vowels_uppercase = AEUY
# cat=Password/170/70_numbers; type=int; label=Password length: This is the password length in characters: One number will be used at random position
password.numbers = 23456789
# cat=Password/170/80_otherchars; type=string; label=Other characters: One special char will be used at random position
password.otherchars = @#$%

# cat=Site_Configuration/160/10_siteConfigurationDefaults; type=string; label=Site Configuration defaults path: Configuration File path (EXT:my_ext/Configuration/Site/Default.yaml)
siteConfiguration.defaults = EXT:site_generator_extended/Configuration/Site/Default.yaml
# cat=Site_Configuration/160/20_siteConfigurationTemplate; type=string; label=Site Configuration template path: Configuration File path (EXT:my_ext/Configuration/Site/Template.yaml)
siteConfiguration.template = EXT:site_generator_extended/Configuration/Site/Template.yaml
# cat=Site_Configuration/160/30_includeLanguageSettings; type=boolean; label=Include language settings: if not they should be included with default yaml configuration
siteConfiguration.includeLanguageSettings = 1
# cat=Site_Configuration/160/40_404Pid; type=int; label=404 page id: The original uid of 404 page
siteConfiguration.404Pid =
# cat=Site_Configuration/160/40_CookiesInformationPid; type=int; label=Cookie information pid: The original uid of the cookie information page
siteConfiguration.cookiesInformationPid =
# cat=Site_Configuration/160/50_CookiesInformationRoute; type=string; label=Cookie information route: What the route cookieInfo route should be
siteConfiguration.cookiesInformationRoute = /cookieInfo/
# cat=Site_Configuration/160/60_BaseVariantStagingDomain; type=string; label=Base Variant Staging Domain: The domain to be used for staging base variant
siteConfiguration.baseVariants.staging.domain =
# cat=Site_Configuration/160/60_BaseVariantTestingDomain; type=string; label=Base Variant Testing Domain: The domain to be used for testing base variant
siteConfiguration.baseVariants.testing.domain =
# cat=Site_Configuration/160/60_BaseVariantUpgradeDomain; type=string; label=Base Variant Upgrade Domain: The domain to be used for upgrade base variant
siteConfiguration.baseVariants.upgrade.domain =
# cat=Site_Configuration/160/60_BaseVariantDevelopmentDomain; type=string; label=Base Variant Development Domain: The domain to be used for development base variant
siteConfiguration.baseVariants.development.domain =